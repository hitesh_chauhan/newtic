//
//  gamePlay.h
//  tictactoeproject
//
//  Created by Click Labs108 on 10/15/15.
//  Copyright © 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol gamePlayClassDelegate


-(void)gamePlayBack;
@end

@interface gamePlay : UIView

@property (nonatomic, assign)id <gamePlayClassDelegate> delegateFirst;

-(void) playersnames;
-(void)buttons;
@property(nonatomic,strong) UILabel*firstPlayer;


@end
