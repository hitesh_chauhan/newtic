//
//  HomeView.h
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.



#import <UIKit/UIKit.h>

@protocol ButtonProtocolName <NSObject>

-(void) buttonWasPressed;



@end
@interface HomeView : UIView




 
@property (nonatomic, assign) id <ButtonProtocolName> delegate;
-(void)buttons;
-(void)startButtonAction;
-(void)scoreButtonAction;
-(void)settingButtonAction;
-(void)avAudioAction;



@end
