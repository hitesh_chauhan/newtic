//
//  PlayersView.h
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol startbuttonClassDelegate 


-(void)startbuttonPresses;
-(void)backbuttonpressed;




@end
@interface PlayersView : UIView

@property (nonatomic, assign)id <startbuttonClassDelegate> delegateFirst;



-(void)textFields;
-(void)Button;
-(void)backButton;
@end
