//
//  gamePlay.m
//  tictactoeproject
//
//  Created by Click Labs108 on 10/15/15.
//  Copyright © 2015 tashanpunjabi. All rights reserved.
//

#import "gamePlay.h"
#import "PlayersView.h"
#import "ViewController.h"

@implementation gamePlay

{
    UILabel*secondPlayer;
    UIView*lineView;
    UIView*lineView2;
    UIView*lineView3;
    UIView*lineView4;
    UIView*lineView5;
    UIView*lineView6;
    UILabel*firstscore;
    UILabel*secondScore;
    ViewController*mainController;
    UIButton*firstboxbutton;
    UIButton*secondboxbutton;
    UIButton*thirdboxbutton;
    UIButton*forthboxbutton;
    UIButton*fifthboxbutton;
    UIButton*sixthboxbutton;
    UIButton*seventhboxbutton;
    UIButton*eightboxbutton;
    UIButton*ninethboxbutton;
    
    int p;
    int gameState ;
    int winningCombinations;
}

-(void) playersnames{
  
    p=1;
    _firstPlayer=[[UILabel alloc]initWithFrame:CGRectMake(10  ,48, 130, 30)];
  
    _firstPlayer.textColor=[UIColor brownColor];
    _firstPlayer.layer.borderWidth=1.5f;
    [[_firstPlayer layer] setBorderColor:[UIColor brownColor].CGColor];
    [self addSubview:_firstPlayer];
    
    secondPlayer=[[UILabel alloc]initWithFrame:CGRectMake(180  ,48, 130, 30)];
    secondPlayer.textColor=[UIColor brownColor];
    [[secondPlayer layer] setBorderColor:[UIColor brownColor].CGColor];

    secondPlayer.layer.borderWidth=1.5f;
    [self addSubview:secondPlayer];
    
    
    
    lineView=[[UIView alloc]initWithFrame:CGRectMake(100, 100, 2, 300)];
    lineView.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView];
    
    
    
    lineView2=[[UIView alloc]initWithFrame:CGRectMake(210, 100, 2, 300)];
    lineView2.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView2];
    
    
    lineView3=[[UIView alloc]initWithFrame:CGRectMake(0, 200, 320, 2)];
    lineView3.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView3];
    
    
    
    lineView4=[[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 2)];
    lineView4.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView4];
    
    
    lineView5=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 320, 2)];
    lineView5.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView5];
    
    
    
    lineView6=[[UIView alloc]initWithFrame:CGRectMake(0, 400, 320, 2)];
    lineView6.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView6];
    
    
    
    firstscore=[[UILabel alloc]initWithFrame:CGRectMake(10  ,460, 60, 40)];
    
    firstscore.textColor=[UIColor brownColor];
    [[firstscore layer] setBorderColor:[UIColor brownColor].CGColor];

    firstscore.layer.borderWidth=1.5f;
    
    [self addSubview:firstscore];
    
    secondScore=[[UILabel alloc]initWithFrame:CGRectMake(250  ,460, 60, 40)];
    secondScore.textColor=[UIColor brownColor];
    [[secondScore layer] setBorderColor:[UIColor brownColor].CGColor];

    secondScore.layer.borderWidth=1.5f;
    [self addSubview:secondScore];
    
    
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(10  ,18, 80, 22)];
    backButton.backgroundColor=[UIColor lightGrayColor];
    [[backButton layer] setBorderColor:[UIColor brownColor].CGColor];
    backButton.layer.borderWidth=2.0f;
    [backButton setTitle:@"BACK" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
    
    UIButton*resetButton=[[UIButton alloc]initWithFrame:CGRectMake(120  ,440, 80, 75)];
    resetButton.userInteractionEnabled=TRUE;
    [resetButton setImage:[UIImage imageNamed:@"resetimage.png"]forState:UIControlStateNormal];
    [resetButton setTitle:@"RESET" forState:UIControlStateNormal];
    resetButton.titleLabel.textAlignment=NSTextAlignmentNatural;
    [resetButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [resetButton addTarget:self action:@selector(resetButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:resetButton];
    
    
    
    
    
}

-(void)backButtonAction{
    [self.delegateFirst gamePlayBack];
    
    
}

-(void)resetButtonAction{
    
    
    
}



-(void)buttons{
    
    
    firstboxbutton =[[UIButton alloc]initWithFrame:CGRectMake(0  ,102, 100, 100)];
   
    [[firstboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    firstboxbutton.tag=101;
    firstboxbutton.layer.borderWidth=2.0f;
  
  
    [firstboxbutton addTarget:self action:@selector(firstboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:firstboxbutton];
    
    secondboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(0  ,202, 100, 100)];

    [[secondboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    firstboxbutton.layer.borderWidth=2.0f;
    secondboxbutton.tag=102;


    [secondboxbutton addTarget:self action:@selector(secondboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:secondboxbutton];
    
    
    thirdboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(102 ,302, 100, 100)];
  
    [[thirdboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    thirdboxbutton.layer.borderWidth=2.0f;
    thirdboxbutton.tag=103;

  
    [thirdboxbutton addTarget:self action:@selector(thirdboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:thirdboxbutton];
    
    forthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(102 ,102, 100, 100)];
    
    [[forthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    forthboxbutton.layer.borderWidth=2.0f;
    forthboxbutton.tag=111;

    
    [forthboxbutton addTarget:self action:@selector(forthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:forthboxbutton];
    
    
    fifthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(102 ,202, 100, 100)];
    
    [[fifthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    fifthboxbutton.layer.borderWidth=2.0f;
    fifthboxbutton.tag=112;

    
    [fifthboxbutton addTarget:self action:@selector(fifthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fifthboxbutton];
    
    sixthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(102 ,302, 100, 100)];
    
    [[sixthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    sixthboxbutton.layer.borderWidth=2.0f;
    sixthboxbutton.tag=113;

    
    [sixthboxbutton addTarget:self action:@selector(sixthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sixthboxbutton];
    
    seventhboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(202 ,102, 100, 100)];
    
    [[seventhboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    seventhboxbutton.layer.borderWidth=2.0f;
    
    
    [seventhboxbutton addTarget:self action:@selector(seventhboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:seventhboxbutton];
    seventhboxbutton.tag=121;

    eightboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(202 ,202, 100, 100)];
    
    [[eightboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    eightboxbutton.layer.borderWidth=2.0f;
    
    eightboxbutton.tag=122;

    [eightboxbutton addTarget:self action:@selector(eightboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:eightboxbutton];
    
    ninethboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(202 ,302, 100, 100)];
    
    [[ninethboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    ninethboxbutton.layer.borderWidth=2.0f;
    ninethboxbutton.tag=123;

    
    [ninethboxbutton addTarget:self action:@selector(ninethboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:ninethboxbutton];
    
    
}

-(void)firstboxButtonAction{
    if(p==1){
    
     firstboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    else if (p==2)
        firstboxbutton.backgroundColor=[UIColor redColor];
    p=1;
}

-(void)secondboxButtonAction{
      if(p==1){
    
    secondboxbutton.backgroundColor=[UIColor greenColor];
          p=2;
}

      else if (p==2){
          secondboxbutton.backgroundColor=[UIColor redColor];
          p=1;
      }
}
    -(void)thirdboxButtonAction{
        
        if(p==1){
            
            thirdboxbutton.backgroundColor=[UIColor greenColor];
            p=2;
        }
        
        else if (p==2){
            thirdboxbutton.backgroundColor=[UIColor redColor];
            p=1;
        }
        
    }

-(void)forthboxbuttonAction{
    if(p==1){
        
        forthboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        forthboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
}
-(void)fifthboxbuttonAction{
    if(p==1){
        
        fifthboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        fifthboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
    
}
-(void)sixthboxbuttonAction{
    
    
    if(p==1){
        
        sixthboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        sixthboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
    
}

-(void)seventhboxbuttonAction{
    
    if(p==1){
        
        seventhboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        seventhboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
    
    
}

-(void)eightboxbuttonAction{
    if(p==1){
        
        eightboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        eightboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
    
}
-(void)ninethboxbuttonAction{
    
    if(p==1){
        
        ninethboxbutton.backgroundColor=[UIColor greenColor];
        p=2;
    }
    
    else if (p==2){
        ninethboxbutton.backgroundColor=[UIColor redColor];
        p=1;
    }
}


@end


